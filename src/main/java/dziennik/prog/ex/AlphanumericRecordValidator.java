package dziennik.prog.ex;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class AlphanumericRecordValidator implements RecordValidator
{

    private static final String alphaExpression = "^[a-zA-Z\\s]+";

    public boolean isValidMark(int mark)
    {
        return (mark >= 2 && mark <= 5);
    }

    public boolean isValidLastName(String lastName)
    {
        return(lastName != null && !lastName.equals("") && lastName.length() < 30 && lastName.matches(alphaExpression));
    }

    public boolean isValidFirstName(String name)
    {
        return (name != null && !name.equals("") && name.length() < 15 && name.matches(alphaExpression));
    }
}
