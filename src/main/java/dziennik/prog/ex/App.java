package dziennik.prog.ex;


import java.util.List;

public class App
{
    public static void main( String[] args )
    {
        new App().runApp();
    }

    private void runApp()
    {
        loadRecordFromFile();
        Menu menu = new Menu(loadRecordFromFile());
        menu.displayWelcomeMessage();
        menu.displayMenu();
        saveRecordsToFile(menu.getRecords());
    }

    private List<Record> loadRecordFromFile()
    {
        FileService fileService = FileServiceFactory.getDefaultFileService("lista.csv");
        return fileService.readRecordsFromFile();
    }

    private boolean saveRecordsToFile(List<Record> records)
    {
        FileService fileService = FileServiceFactory.getDefaultFileService("lista.csv");
        return fileService.saveRecordsToFile(records);
    }
}
