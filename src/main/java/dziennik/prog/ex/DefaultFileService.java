package dziennik.prog.ex;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public final class DefaultFileService implements FileService {
    private String fileName;

    public DefaultFileService(String fileName)
    {
        this.fileName = fileName;
    }

    public List<Record> readRecordsFromFile()
    {
        List<Record> recordToReturn = new ArrayList<>();

        try
        {
            List<String[]> lines = Files.readAllLines(Paths.get(fileName))
                    .stream().map(p -> p.split(",")).collect(Collectors.toList());
            for (String[] record : lines)
            {
                recordToReturn.add(new Record(record[0], record[1], Integer.parseInt(record[2]), record[3]));
            }
        } catch (IOException e)
        {
            System.out.println("Wystapil blad podczas pobierania wpisow.");
        }
        return recordToReturn;
    }


    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean saveRecordsToFile(List<Record> records)
    {
        try
        {
            PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
            for (Record record : records)
            {
                pw.println(record.toString());
            }
            pw.close();
        } catch (FileNotFoundException e)
        {
            return false;
        }
        return true;
    }
}
