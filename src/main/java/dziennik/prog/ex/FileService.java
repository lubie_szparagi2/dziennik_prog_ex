package dziennik.prog.ex;


import java.util.List;

public interface FileService
{
    List<Record> readRecordsFromFile();

    boolean saveRecordsToFile(List<Record> records);
}
