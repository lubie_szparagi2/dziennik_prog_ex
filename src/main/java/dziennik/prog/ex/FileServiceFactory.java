package dziennik.prog.ex;


public class FileServiceFactory
{
    public static DefaultFileService getDefaultFileService(String filename)
    {
        return new DefaultFileService(filename);
    }
}
