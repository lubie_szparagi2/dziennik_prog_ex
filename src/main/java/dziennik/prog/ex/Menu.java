package dziennik.prog.ex;


import java.util.List;
import java.util.Scanner;

public class Menu {

    private List<Record> recordList;

    public Menu(List<Record> recordList)
    {
        this.recordList = recordList;
    }

    public void displayWelcomeMessage()
    {
        System.out.println(MenuMessages.getWelcomeMsg());
    }

    public void displayMenuMessages()
    {
        System.out.println(MenuMessages.getShowEntries());
        System.out.println(MenuMessages.getAddEntry());
        System.out.println(MenuMessages.getRemoveEntry());
        System.out.println(MenuMessages.getQuit());
    }

    public String getWrongSelectionError()
    {
        return MenuMessages.getWrongSelection();
    }

    public String getExitMsg()
    {
        return MenuMessages.getExitMsg();
    }

    public void displayMenu()
    {
        Scanner scanner = new Scanner(System.in);
        char choice = ' ';

        while(choice != 'q')
        {
            displayMenuMessages();
            System.out.println(MenuMessages.getPrompt());
            choice = scanner.next().charAt(0);

            String toDisplay = whatToDisplay(choice);
            if(toDisplay != null) {
                System.out.println(toDisplay);
            }
        }
    }

    public String whatToDisplay(char choice) {
        switch(choice)
		{
			case '1':
				return showRecords();
			case '2':
				readRecord();
				return null;
			case '3':
				readDeleteRecord();
				return null;
			case 'q':
				return getExitMsg();
			default:
				return getWrongSelectionError();
		}
    }

    public String showRecords()
    {
        return MenuMessages.printRecordList(recordList);
    }

    public void addRecord(Record record)
    {
        recordList.add(record);
    }

    public void readRecord()
    {
        Record record = new Record();
        new RecordInserter().insertGrade(record);
        addRecord(record);
    }

    public void deleteRecord(int toDelete)
    {
        recordList.remove(toDelete);
    }

    public void readDeleteRecord()
    {
        System.out.println(MenuMessages.getDeletePrompt());
        Scanner scanner = new Scanner(System.in);
        int toDelete = scanner.nextInt();
        deleteRecord(toDelete-1);
        System.out.println(MenuMessages.getDeleteConfirmation() + toDelete);
    }

    public List<Record> getRecords()
    {
        return recordList;
    }
}
