package dziennik.prog.ex;


import java.util.List;

public class MenuMessages
{
    public static final String welcomeMsg = "\n\nDziennik ucznia.\nProjekt wykonany na zajecia" +
            " z Programowania Ekstremalnego.\nWybierz pozycje z menu:\n\n";
    public static final String showEntries = "\n [1] Wyswietl wpisy";
    public static final String addEntry = " [2] Dodaj wpis";
    public static final String removeEntry = " [3] Usun wpis";
    public static final String quit = " [q] Zamknij aplikacje";
    public static final String wrongSelection = "Nieprawidlowy wybor, wybierz jeszcze raz";
    public static final String prompt = "\n\n Twoj wybor: ";
    public static final String deletePrompt = "Podaj nr wpisu, ktory chcesz usunac";
    public static final String deleteConfirmation = "Usunieto wpis nr ";
    public static final String exitMsg = "Zakonczono dzialanie aplikacji w poprawny sposob";

    public static String getWelcomeMsg()
    {
        return welcomeMsg;
    }

    public static String getShowEntries()
    {
        return showEntries;
    }

    public static String getAddEntry()
    {
        return addEntry;
    }

    public static String getRemoveEntry()
    {
        return removeEntry;
    }

    public static String getQuit()
    {
        return quit;
    }

    public static String getPrompt()
    {
        return prompt;
    }

    public static String getWrongSelection()
    {
        return wrongSelection;
    }

    public static String getDeletePrompt()
    {
        return deletePrompt;
    }

    public static String getDeleteConfirmation()
    {
        return deleteConfirmation;
    }

    public static String getExitMsg()
    {
        return exitMsg;
    }

    public static String printRecordList(List<Record> recordList)
    {
        StringBuilder stringBuffer = new StringBuilder();
        for(int i = 0; i < recordList.size(); i++)
        {
            stringBuffer.append("[").append(i + 1).append("] ").append(recordList.get(i).toString()).append("\n");
        }
        return stringBuffer.toString();
    }
}
