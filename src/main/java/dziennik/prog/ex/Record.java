package dziennik.prog.ex;

import java.util.Objects;

public class Record
{
    private String name;
    private String lastName;
    private int mark;
    private String date;

    public Record()
    {

    }

    public Record(String name, String lastName, int mark, String date)
    {
        this.name = name;
        this.lastName = lastName;
        this.mark = mark;
        this.date = date;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)  { this.name = name; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public int getMark()
    {
        return mark;
    }

    public String getDate()
    {
        return date;
    }

    public void setMark(int mark) { this.mark = mark; }

    public void setDate(String date)
    {
        this.date = date;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Record)) return false;
        Record record = (Record) o;
        return Double.compare(record.getMark(), getMark()) == 0 &&
                Objects.equals(getName(), record.getName()) &&
                Objects.equals(getLastName(), record.getLastName()) &&
                Objects.equals(getDate(), record.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getLastName(), getMark(), getDate());
    }

    @Override
    public String toString()
    {
        return name + ',' + lastName + ',' + mark +','+ date;
    }
}
