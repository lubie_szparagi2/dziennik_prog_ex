package dziennik.prog.ex;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class RecordInserter
{
    RecordValidator recordValidator = RecordValidatorFactory.getAlphanumericRecordValidator();

    public void insertGrade(Record record)
    {
        String userInput;
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj imie ucznia");
        while(true)
        {
            userInput = input.nextLine();

            if(recordValidator.isValidFirstName(userInput))
                break;

            System.out.println("Bledne imie, podaj poprawne imie");
        }

        record.setName(userInput);

        System.out.println("Podaj nazwisko ucznia");
        while(true)
        {
            userInput = input.nextLine();

            if(recordValidator.isValidLastName(userInput))
                break;

            System.out.println("Bledne nazwisko, podaj poprawne nazwisko");
        }

        record.setLastName(userInput);

        System.out.println("Podaj ocene");
        while(true)
        {
            try
            {
                int mark = Integer.parseInt(input.nextLine());
                if(recordValidator.isValidMark(mark))
                {
                    record.setMark(mark);
                    break;
                }
            }
            catch(Exception ex)
            {
                System.out.println("Bledna ocena, podaj poprawna ocena");
            }
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        record.setDate(simpleDateFormat.format(date));
    }
}
