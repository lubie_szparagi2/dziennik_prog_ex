package dziennik.prog.ex;


public interface RecordValidator
{
    boolean isValidMark(int mark);

    boolean isValidLastName(String lastName);

    boolean isValidFirstName(String name);
}
