package dziennik.prog.ex.autom;

import java.util.ArrayList;

import dziennik.prog.ex.Menu;
import dziennik.prog.ex.MenuMessages;
import dziennik.prog.ex.Record;

public class AddRecordTest {
	private String name;
	private String lastName;
	private int mark;
	private String date;

	private String result;

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void execute() {
		Menu menu = new Menu(new ArrayList<>());
		menu.addRecord(new Record(name, lastName, mark, date));
		result = MenuMessages.printRecordList(menu.getRecords());
	}

	public String result(){
		return result;
	}

}
