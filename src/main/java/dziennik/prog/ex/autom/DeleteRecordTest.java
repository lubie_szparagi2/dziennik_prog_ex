package dziennik.prog.ex.autom;

import java.util.ArrayList;
import java.util.Arrays;

import dziennik.prog.ex.Menu;
import dziennik.prog.ex.MenuMessages;
import dziennik.prog.ex.Record;

public class DeleteRecordTest {
	private String name;
	private String lastName;
	private int mark;
	private String date;
	private int indexToDelete;

	private String result;

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getIndexToDelete() {
		return indexToDelete;
	}

	public void setIndexToDelete(int indexToDelete) {
		this.indexToDelete = indexToDelete;
	}

	public void execute() {
		Menu menu = new Menu(new ArrayList<>(Arrays.asList(new Record(name, lastName, mark, date))));
		menu.deleteRecord(0);
		result = MenuMessages.printRecordList(menu.getRecords());
	}

	public String result(){
		return result;
	}

}
