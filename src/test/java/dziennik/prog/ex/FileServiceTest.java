package dziennik.prog.ex;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class FileServiceTest {

    @Test
    public void readRecordsFromFile() throws Exception
    {
        DefaultFileService fileService = FileServiceFactory.getDefaultFileService("test-input-test.csv");
        String dateInString = "2017-04-26";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(dateInString);
        Date date2 = formatter.parse("2017-04-27");

        Record record = new Record("Tomek", "Kowalski", 2, formatter.format(date));
        Record record2 = new Record("Jacek", "Nowak", 3, formatter.format(date2));

        List<Record> records = fileService.readRecordsFromFile();
        assertEquals(record, records.get(0));
        assertEquals(record2, records.get(1));
    }

    @Test
    public void saveRecordsToFile() throws Exception
    {
        DefaultFileService fileService = FileServiceFactory.getDefaultFileService("test-input-test2.csv");
        String dateInString = "2017-04-26";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(dateInString);


        Record record = new Record("Piotr", "Kowalski", 4, formatter.format(date));
        List<Record> records = new ArrayList<>();
        records.add(record);

        boolean isSuccess = fileService.saveRecordsToFile(records);
        List<Record> recordsFromFile = fileService.readRecordsFromFile();
        assertTrue(isSuccess);
        assertThat(records, is(recordsFromFile));
    }
}
