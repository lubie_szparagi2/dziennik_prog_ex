package dziennik.prog.ex;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MenuTest
{

    @Test
    public void testWelcomeMsg()
    {
        String welcomeMsg = "\n\nDziennik ucznia.\nProjekt wykonany na zajecia" +
                " z Programowania Ekstremalnego.\nWybierz pozycje z menu:\n\n";
        assertTrue(welcomeMsg.equals(MenuMessages.getWelcomeMsg()));
    }

    @Test
    public void testShowEntries()
    {
        String showEntries = "\n [1] Wyswietl wpisy";
        assertTrue(showEntries.equals(MenuMessages.getShowEntries()));
    }

    @Test
    public void testAddEntry()
    {
        String addEntry = " [2] Dodaj wpis";
        assertTrue(addEntry.equals(MenuMessages.getAddEntry()));
    }

    @Test
    public void testRemoveEntry()
    {
        String removeEntry = " [3] Usun wpis";
        assertTrue(removeEntry.equals(MenuMessages.getRemoveEntry()));
    }

    @Test
    public void testQuit()
    {
        String quit = " [q] Zamknij aplikacje";
        assertTrue(quit.equals(MenuMessages.getQuit()));
    }

    @Test
    public void testPrompt()
    {
        String prompt = "\n\n Twoj wybor: ";
        assertTrue(prompt.equals(MenuMessages.getPrompt()));
    }

    @Test
    public void testWrongSelection()
    {
        String wrongSelection = "Nieprawidlowy wybor, wybierz jeszcze raz";
        assertTrue(wrongSelection.equals(MenuMessages.getWrongSelection()));
    }

    @Test
    public void testDeletePrompt()
    {
        String deletePrompt = "Podaj nr wpisu, ktory chcesz usunac";
        assertTrue(deletePrompt.equals(MenuMessages.getDeletePrompt()));
    }

    @Test
    public void testDeleteConfirmation()
    {
        String deleteConfirmation = "Usunieto wpis nr ";
        assertTrue(deleteConfirmation.equals(MenuMessages.getDeleteConfirmation()));
    }

    @Test
    public void testExitMsg()
    {
        String exitMsg = "Zakonczono dzialanie aplikacji w poprawny sposob";
        assertTrue(exitMsg.equals(MenuMessages.getDeleteConfirmation()));
    }
}
