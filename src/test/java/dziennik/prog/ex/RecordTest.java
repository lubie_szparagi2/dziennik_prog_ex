package dziennik.prog.ex;


import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class RecordTest
{
    private Record record;
    private ArrayList<Record> records;

    @Before
    public void setUp()
    {
        records = new ArrayList<>();
        record = new Record("name", "lastName", 4, "2017-04-23");
    }

    @Test
    public void addRecordToListTest()
    {
        records.add(record);
        assertEquals(records.size(), 1);
    }

    @Test
    public void testFirstName()
    {
        AlphanumericRecordValidator testObject = new AlphanumericRecordValidator();
        assertFalse( testObject.isValidFirstName("") );
        assertFalse( testObject.isValidFirstName(null) );
        assertFalse( testObject.isValidFirstName("abcdefghijklmnoprstowyzabcdefghijkalmasdnasiuagbisuaguisbuiabfgiuas") );
    }

    @Test
    public void testLastName()
    {
        AlphanumericRecordValidator testObject = new AlphanumericRecordValidator();
        assertTrue( testObject.isValidFirstName("Bartek"));
        assertFalse( testObject.isValidLastName("") );
        assertFalse( testObject.isValidLastName(null) );
        assertFalse( testObject.isValidLastName("EkierTTYYRWTWE@!@!#@!@#@!#"));
        assertFalse( testObject.isValidLastName("abcdefghijklmnoprstowyzabcdefghijkalmasdnasiuagbisuaguisbuiabfgiuas") );
    }

    @Test
    public void testGrade()
    {
        AlphanumericRecordValidator testObject = new AlphanumericRecordValidator();
        assertFalse( testObject.isValidMark(-1) );
        assertFalse( testObject.isValidMark(9) );
        assertTrue( testObject.isValidMark(5) );
    }

}
